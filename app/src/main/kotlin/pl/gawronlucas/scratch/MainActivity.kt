package pl.gawronlucas.scratch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.support.v7.widget.Toolbar
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.Menu
import android.view.MenuItem
import org.jetbrains.anko.find
import pl.gawronlucas.scratch.core.Note
import pl.gawronlucas.scratch.view.NotesListAdapter
import pl.gawronlucas.scratch.view.NotesPresenter
import pl.gawronlucas.scratch.view.NotesPresenterImpl
import pl.gawronlucas.scratch.view.NotesView

class MainActivity : AppCompatActivity(), NotesView {
    lateinit var notesPresenter: NotesPresenter

    val notesList: RecyclerView by lazy { find<RecyclerView>(R.id.notes_list) }
    val notesListAdapter: NotesListAdapter by lazy { NotesListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setToolbar(R.id.toolbar)
        setPresenter()
        setNotesList()
    }

    override fun onStart() {
        super.onStart()
        notesPresenter.loadNotes()
    }

    override fun onStop() {
        super.onStop()
    }

    fun setToolbar(id: Int) {
        val toolbar = find<Toolbar>(id)
        setSupportActionBar(toolbar)
    }

    fun setPresenter() {
        notesPresenter = NotesPresenterImpl()
        notesPresenter.bindView(this)
    }

    fun setNotesList() {
        notesList.layoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        val notesTouchHelper = ItemTouchHelper(object : ItemTouchHelper.Callback() {
            override fun getMovementFlags(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?): Int {
                return makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)
            }

            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return true
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder?, direction: Int) {
            }

            override fun isItemViewSwipeEnabled(): Boolean = notesListAdapter.isItemSwipeEnabled()
            override fun isLongPressDragEnabled(): Boolean = false
        })
        notesTouchHelper.attachToRecyclerView(notesList)
        notesList.adapter = notesListAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        notesPresenter.unbindView()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> {
                return true
            }
            R.id.action_change_list_display -> {
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // NotesView methods
    ///////////////////////////////////////////////////////////////////////////

    override fun showNoteDetails() {
    }

    override fun createNewNote() {
    }

    override fun archiveNote() {
    }

    override fun deleteNote() {
    }

    override fun selectNote() {
    }

    override fun searchNotes() {
    }

    override fun refreshNotesList() {
    }

    override fun showNoContent() {
        if (!notesListAdapter.items.isEmpty()) {
            (notesList.layoutManager as StaggeredGridLayoutManager).spanCount = 1
            notesListAdapter.updateNotes(mutableListOf<Note>()) // update empty list will show empty message
        }
    }

    override fun showNotesList(notes: List<Note>) {
        (notesList.layoutManager as StaggeredGridLayoutManager).spanCount = 2
        notesListAdapter.updateNotes(notes.toMutableList())
    }
}
