package pl.gawronlucas.scratch.data

import pl.gawronlucas.scratch.core.Note
import pl.gawronlucas.scratch.core.Repository

/**
 * Created by Lucas on 2016-12-26.
 */
interface NotesRepository : Repository<Note, Long> {
    fun findByTitle(title: String): List<Note>
    fun findByLabel(label: String): List<Note>
    fun findByType(type: Note.Type): List<Note>
    fun getAll(): List<Note>
}