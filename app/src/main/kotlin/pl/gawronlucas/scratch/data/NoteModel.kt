package pl.gawronlucas.scratch.data

import com.raizlabs.android.dbflow.annotation.*
import pl.gawronlucas.scratch.core.Note
import pl.gawronlucas.scratch.db.ScratchDatabase

/**
 * Created by Lucas on 2016-12-28.
 */
@Table(database = ScratchDatabase::class, name = "Notes")
@ManyToMany(referencedTable = NoteLabelModel::class)
open class NoteModel(@PrimaryKey var id: Long,
                     @ForeignKey var parentId: Long,
                     @Column var title: String = "",
                     @Column var body: String = "",
                     @Column var modificationDate: Long = 0,
                     @Column var labels: List<String> = listOf<String>(),
                     @Column var color: Int = 0xffffffff.toInt(),
                     @Column var type: Note.Type = Note.Type.TEXT)