package pl.gawronlucas.scratch.data

import pl.gawronlucas.scratch.core.Note

/**
 * Created by Lucas on 2016-12-26.
 *
 * // TODO: 2016-12-26 implement sql db
 */
class SqliteNotesRepository : NotesRepository {

    val mapper = NoteMapper()

    override fun getAll(): List<Note> {
        return listOf()
    }

    override fun findByTitle(title: String): List<Note> {
        return listOf()
    }

    override fun findByLabel(label: String): List<Note> {
        return listOf()
    }

    override fun findByType(type: Note.Type): List<Note> {
        return listOf()
    }

    override fun get(id: Long): Note {
        return Note(0)
    }

    override fun save(item: Note) {
    }

    override fun delete(item: Note) {
    }
}