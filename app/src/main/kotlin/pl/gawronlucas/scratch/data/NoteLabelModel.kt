package pl.gawronlucas.scratch.data

import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import pl.gawronlucas.scratch.db.ScratchDatabase

/**
 * Created by Lucas on 2016-12-28.
 */
@Table(database = ScratchDatabase::class, name = "NoteLabel")
class NoteLabelModel(@PrimaryKey var id: Long, var label: String)