package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-26.
 */
@Entity
class NoteGroup(id: Long,
                title: String,
                body: String,
                modificationDate: Long,
                labels: List<String>,
                color: Int,
                var notes: MutableList<Note> = mutableListOf()) : Note(id, title, body, modificationDate, labels, color, Type.TEXT) {

    fun addNote(note: Note) {
        this.notes.add(note)
    }

    fun removeNote(note: Note) {
        this.notes.remove(note)
    }
}