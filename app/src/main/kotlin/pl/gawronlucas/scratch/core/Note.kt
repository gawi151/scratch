package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-26.
 */
@Entity
open class Note(var id: Long,
                var title: String = "",
                var body: String = "",
                var modificationDate: Long = 0,
                var labels: List<String> = listOf<String>(),
                var color: Int = 0xffffffff.toInt(),
                var type: Type = Note.Type.TEXT) {

    enum class Type {
        TEXT
    }
}