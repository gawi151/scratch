package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-28.
 */
interface TwoWayMapper<T, R> : Mapper<T, R> {
    fun mapFrom(r: R): T
}