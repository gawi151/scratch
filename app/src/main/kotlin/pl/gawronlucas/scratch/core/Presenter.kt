package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-26.
 */
interface Presenter<in T : View> {
    fun bindView(view: T)
    fun unbindView()
}