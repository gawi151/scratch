package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-26.
 */
interface Repository<T, in K> {
    fun get(id: K): T
    fun save(item: T)
    fun delete(item: T)
}