package pl.gawronlucas.scratch.core

/**
 * Created by Lucas on 2016-12-28.
 */
interface Mapper<in T, out R> {
    fun mapTo(t: T): R
}