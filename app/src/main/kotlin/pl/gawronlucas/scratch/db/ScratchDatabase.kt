package pl.gawronlucas.scratch.db

import com.raizlabs.android.dbflow.annotation.Database

/**
 * Created by Lucas on 2016-12-26.
 */
@Database(version = ScratchDatabase.VERSION, name = ScratchDatabase.NAME)
class ScratchDatabase {

    companion object {
        const val VERSION = 1
        const val NAME = "ScratchDatabase"
    }
}