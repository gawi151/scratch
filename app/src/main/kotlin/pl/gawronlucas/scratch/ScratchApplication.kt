package pl.gawronlucas.scratch

import android.app.Application
import com.raizlabs.android.dbflow.config.FlowManager
import com.raizlabs.android.dbflow.config.FlowConfig

/**
 * Created by Lucas on 2016-12-26.
 */
class ScratchApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        FlowManager.init(FlowConfig.Builder(this)
                .openDatabasesOnInit(true).build())
    }
}