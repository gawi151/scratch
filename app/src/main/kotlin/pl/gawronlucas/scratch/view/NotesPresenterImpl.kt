package pl.gawronlucas.scratch.view

import pl.gawronlucas.scratch.data.NotesRepository
import pl.gawronlucas.scratch.data.SqliteNotesRepository

/**
 * Created by Lucas on 2016-12-26.
 */
class NotesPresenterImpl : NotesPresenter {

    var notesView: NotesView? = null
    var notesRepository: NotesRepository = SqliteNotesRepository()

    override fun bindView(view: NotesView) {
        this.notesView = view
    }

    override fun unbindView() {
        this.notesView = null
    }

    override fun loadNotes() {
        notesView?.let {
            // TODO: 2016-12-26 make async
            val notes = notesRepository.getAll()
            if (notes.isEmpty()) it.showNoContent()
            else it.showNotesList(notes)
        }
    }
}