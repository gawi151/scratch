package pl.gawronlucas.scratch.view

/**
 * Created by Lucas on 2016-12-26.
 */
interface OnRecycleViewHolder {
    fun onRecycle()
}