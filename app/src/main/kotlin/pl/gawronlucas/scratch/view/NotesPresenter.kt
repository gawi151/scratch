package pl.gawronlucas.scratch.view

import pl.gawronlucas.scratch.core.Presenter

/**
 * Created by Lucas on 2016-12-26.
 */
interface NotesPresenter : Presenter<NotesView> {
    fun loadNotes()
}