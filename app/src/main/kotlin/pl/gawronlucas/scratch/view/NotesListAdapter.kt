package pl.gawronlucas.scratch.view

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import pl.gawronlucas.scratch.core.Note

/**
 * Created by Lucas on 2016-12-26.
 */
class NotesListAdapter(var items: MutableList<Note> = mutableListOf()) : RecyclerView.Adapter<NoteViewHolder>() {
    override fun getItemCount(): Int = if (items.isEmpty()) 1 else items.count()

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        if (items.isNotEmpty()) holder.onBind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder.Factory.make(parent, viewType)
    }

    override fun getItemViewType(position: Int): Int {
        if (itemCount == 1 && items.isEmpty()) return NoteViewHolder.TYPE_EMPTY
        else return NoteViewHolder.TYPE_TEXT
    }

    fun updateNotes(notes: MutableList<Note>) {
        this.items = notes
        notifyDataSetChanged()
    }

    fun isItemSwipeEnabled(): Boolean {
        return !(itemCount == 1 && items.isEmpty())
    }
}