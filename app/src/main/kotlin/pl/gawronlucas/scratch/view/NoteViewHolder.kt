package pl.gawronlucas.scratch.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import pl.gawronlucas.scratch.R
import pl.gawronlucas.scratch.core.Note

/**
 * Created by Lucas on 2016-12-26.
 */
abstract class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), OnBindViewHolder<Note> {
    object Factory {
        fun make(parent: ViewGroup, itemType: Int): NoteViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            return when (itemType) {
                TYPE_EMPTY -> {
                    val view = layoutInflater.inflate(R.layout.note_empty_item, parent, false)
                    EmptyNoteViewHolder(view)
                }
                TYPE_TEXT -> {
                    val view = layoutInflater.inflate(R.layout.note_item, parent, false)
                    TextNoteViewHolder(view)
                }
                else -> {
                    val view = layoutInflater.inflate(R.layout.note_item, parent, false)
                    TextNoteViewHolder(view)
                }
            }
        }
    }

    companion object {
        const val TYPE_EMPTY = 0
        const val TYPE_TEXT = 1
    }
}