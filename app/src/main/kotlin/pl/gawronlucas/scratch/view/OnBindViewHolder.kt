package pl.gawronlucas.scratch.view

/**
 * Created by Lucas on 2016-12-26.
 */
interface OnBindViewHolder<in T> {
    fun onBind(item: T)
}