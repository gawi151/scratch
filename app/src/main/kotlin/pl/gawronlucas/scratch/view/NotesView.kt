package pl.gawronlucas.scratch.view

import pl.gawronlucas.scratch.core.Note
import pl.gawronlucas.scratch.core.View

/**
 * Created by Lucas on 2016-12-26.
 */
interface NotesView : View {
    fun showNoteDetails()
    fun createNewNote()
    fun archiveNote()
    fun deleteNote()
    fun selectNote()
    fun searchNotes()
    fun refreshNotesList()
    fun showNoContent()
    fun showNotesList(notes: List<Note>)
}